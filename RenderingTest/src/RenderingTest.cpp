
#include "RenderingTest.h"

//mysql
#include "mysql.h"
#include "my_global.h"

#define SERVER "localhost"
#define USER "root"
#define PASSWORD "081293"
#define DATABASE "test"
//end mysql



//file system
#include "dirent.h" 
#include <sys/stat.h>
#include <fstream>
#include <time.h>
#define is_regular 0100000
//end filesystem

MYSQL *mysqlPtr; // Create a pointer to the MySQL instance

RenderingTest app;


std::ofstream myfile("C:\\Users\\tunce_000\\Documents\\Visual Studio 2010\\Projects\\RenderingTest\\RenderingTest\\res\\bulk.txt");

std::string goldenPlane = "MyPlaneOgre/Earring";
std::string otherPlane = "MyPlaneOgre/Earring2";


std::deque<Ogre::Vector3> linePoints; //points of the the dynamic line

//convert OIS mouse buttons to CEGUI mouse buttons-------------------------------------

CEGUI::MouseButton RenderingTest::convertButton(OIS::MouseButtonID buttonID)
{
    switch (buttonID)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;
 
    case OIS::MB_Right:
        return CEGUI::RightButton;
 
    case OIS::MB_Middle:
        return CEGUI::MiddleButton;
 
    default:
        return CEGUI::LeftButton;
    }
}

bool loadImageFile(const Ogre::String& texture_name, const Ogre::String& texture_path)
{
	Ogre::Image img;
	bool image_loaded = false;
	std::ifstream ifs(texture_path.c_str(), std::ios::binary|std::ios::in);
	if (ifs.is_open())
	{
		Ogre::String tex_ext;
		Ogre::String::size_type index_of_extension = texture_path.find_last_of('.');
		if (index_of_extension != Ogre::String::npos)
		{
			tex_ext = texture_path.substr(index_of_extension+1);
			Ogre::DataStreamPtr data_stream(new Ogre::FileStreamDataStream(texture_path, &ifs, false));
			
			img.load(data_stream, tex_ext);
			Ogre::TextureManager::getSingleton().loadImage(texture_name,
				Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, img, Ogre::TEX_TYPE_2D, 0, 1.0f);
			image_loaded = true;
		}
		ifs.close();
	}
	return image_loaded;
}

void RenderingTest::scanHardDrive (const char* directory)
{
	try
	{
		DIR* pdir = opendir (directory); 
    
		dirent *pent = NULL;

    

		while (pent = readdir (pdir))
		{
		
			if (pent->d_name[0] == '.')
				continue;
		
			//cout << pent->d_name << " - " << pent->d_namlen << endl;
			char* fileName = (char*) malloc(strlen(directory) + strlen(pent->d_name) + 2);
			strcpy_s(fileName, strlen(directory) + 1 ,directory);
			strcat(fileName,  "/");
			strcat(fileName, pent->d_name);

			struct stat st;
			stat(fileName, &st);
        
			if (st.st_mode & is_regular)
			{
				//cout << count << fileName << endl; //get the file

				//OutputDebugString(fileName);
				//OutputDebugString("\n");
				Date d;
			
				app.e->name = pent->d_name;
				app.e->directory = directory;
				app.e->size = st.st_size;
				app.e->full_name = fileName;

				app.e->write_permission = (st.st_mode & 00200) ? 1 : 0;
				app.e->access_permission = (st.st_mode & 00100) ? 1 : 0;
			
				//for successive calls use ctime_s instead of ctime 

				char buffA[50], buffC[50], buffM[50];

				ctime_s(buffC,50,&st.st_ctime);
				ctime_s(buffA,50,&st.st_atime);
				ctime_s(buffM,50,&st.st_mtime);
			
				app.e->creation_time = buffC;
				app.e->access_time = buffA;
				app.e->modification_time = buffM;

				app.e->c = app.e->parseDate(app.e->creation_time);
				app.e->a = app.e->parseDate(app.e->access_time);
				app.e->m = app.e->parseDate(app.e->modification_time);

				app.e->setExtension();

				try
				{
					if (myfile.is_open())
					{
			
						myfile << app.e->name << " < " << app.e->directory << " < "<< app.e->size << " < " << app.e->extension << " < "	<< app.e->write_permission << " < " << app.e->access_permission << " < "
							<< app.e->creation_time << " < " << app.e->access_time << " < " << app.e->modification_time << " < " << app.e->c << " < " << app.e->a << " < "<< app.e->m  <<" > \n";
					}
					else
						OutputDebugStringA("file writing errorrrr\nn\nn\nn");
				}
				catch(std::exception& e)
				{
					OutputDebugString(e.what());
					OutputDebugString("EXCEPTIONNN\nn\n\nn\nn\nn\nn\nn\nn\nn\nn\nn\nn\nn\nn\nn\nn\nn\n\nn\nn\nn\nn\nnn\nn\nEXCEPTIONNN\nn\n");
					
				}
			


				//std::stringstream ss;
				/*ss << "INSERT INTO `test`.`file` ( `name`, `directory`, `size`, `extension`, `write_permission`,`access_permission`, `c_time`,`a_time`,`m_time`,`c`,`a`,`m`) VALUES ("<< "'"
					<< app.e->name << "', " << "'"
					<< app.e->directory << "', " << "'" << app.e->size << "', " << "'" << app.e->extension << "', " << "'" 
					<< app.e->write_permission << "', " << "'" << app.e->access_permission << "', " << "'" 
					<< app.e->creation_time << "', " << "'" << app.e->access_time << "', " << "'" << app.e->modification_time << "', " << "'" << app.e->c << "', " << "'" << app.e->a << "', " << "'" << app.e->m  <<"'); ";
	*/
				//std::string insertQuery = ss.str();

				//OutputDebugString(ss.str().c_str());
				//OutputDebugString("\n");

		
			
			}
			else
			{
				scanHardDrive (fileName);
			}

			free (fileName);
        
		}
		
	}
	catch(std::exception& e)
	{
		OutputDebugString("\n\n\nn\nn\nnGENERAL EXCEPTION \NN\NN\NN\NN");
		OutputDebugString(e.what());

	}
	
	
}

Ogre::Vector3 toVector (Vector leapVector)
{
return Ogre::Vector3(leapVector.x, leapVector.y, leapVector.z);
}

bool RenderingTest::cameraLimits()
{
	if (mCamera->getPosition().x > 50 && mCamera->getPosition().x < 7500 && mCamera->getPosition().y > 90 && mCamera->getPosition().y < 26000 && mCamera->getPosition().z > 200)
		return true;
	else 
		return false;
}


void mysqlConn ()
{
	
    mysqlPtr = mysql_init(NULL); // Initialise the instance
    /* This If is irrelevant and you don't need to show it. I kept it in for Fault Testing.*/
    if(!mysqlPtr)    /* If instance didn't initialize say so and exit with fault.*/
    {
        fprintf(stderr,"MySQL Initialization Failed");
        
    }
    /* Now we will actually connect to the specific database.*/
	
	mysql_options(mysqlPtr,MYSQL_OPT_LOCAL_INFILE,0);

    mysqlPtr=mysql_real_connect(mysqlPtr,SERVER,USER,PASSWORD,DATABASE,0,NULL,0);
 
    //if(mysqlPtr){
    //    printf("Connection Succeeded\n");
    //}
    //else{
    //    printf("Connection Failed!\n");
    //}



}


MYSQL_RES* mysqlExecute(const char* query)
{
	//mysql_query(mysqlPtr, "INSERT INTO `test`.`seniorproject` (`a`, `b`, `c`) VALUES ('5', 't', 'y');");
	int errorcode;
	errorcode = mysql_query(mysqlPtr, query);
	//mysql_query(mysqlPtr,query);
	std::string errorstr;
	errorstr = mysql_error(mysqlPtr);

    return mysql_store_result(mysqlPtr); /* Receive the result and store it in res_set */
 
}


void mysqlDisconnect()
{
	mysql_close(mysqlPtr);
}





//-------------------------------------------------------------------------------------
RenderingTest::RenderingTest(void)
    : mRoot(0),
    mCamera(0),
    mSceneMgr(0),
    mWindow(0),
    mResourcesCfg(Ogre::StringUtil::BLANK),
    mPluginsCfg(Ogre::StringUtil::BLANK),
    mTrayMgr(0),
    mCameraMan(0),
    mDetailsPanel(0),
    mCursorWasVisible(false),
    mShutDown(false),
    mInputManager(0),
    mMouse(0),
    mKeyboard(0)
{
	e = Entity::getInstance();
	textCount = textureCount = cubeCount = planeCount = rectangleCount = 0;
	rendered = false;
}
//-------------------------------------------------------------------------------------
RenderingTest::~RenderingTest(void)
{
    if (mTrayMgr) delete mTrayMgr;
    if (mCameraMan) delete mCameraMan;



    //Remove ourself as a Window listener
    Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
    windowClosed(mWindow);
	mysqlDisconnect();
    delete mRoot;
}


//-------------------------------------------------------------------------------------
Ogre::ManualObject* const RenderingTest::rectangle(std::string matName)
{

	char* name = (char*) malloc (32);
	sprintf(name, "rect%d", rectangleCount++);

	Ogre::ManualObject* rect = mSceneMgr->createManualObject(name);
	rect->begin(matName, Ogre::RenderOperation::OT_TRIANGLE_LIST);

	rect->position(100,100.1,0);
	rect->textureCoord(1,0);
	rect->normal(50,500,50);

	rect->position(0,100.1,100);
	rect->textureCoord(0,1);
	rect->normal(50,500,50);

	rect->position(0,100.1,0);
	rect->textureCoord(0,0);
	rect->normal(50,500,50);

	rect->position(100,100.1,100);
	rect->textureCoord(1,1);
	rect->normal(50,500,50);

	rect->triangle(2,1,0);
	rect->triangle(1,3,0);
	
	rect->end();
	free(name);


	return rect;


}

//-------------------------------------------------------------------------------------
Ogre::ManualObject* const RenderingTest::plane(std::string material)
{
	Ogre::ColourValue cv;
	cv.r = cv.g = cv.b = 1.0;
	
	std::string name;
	std::stringstream ss3;
	ss3 << "planeMesh" << material;
	name = ss3.str();

	Ogre::ManualObject* plane = mSceneMgr->createManualObject(name.c_str());
	plane->begin(material ,Ogre::RenderOperation::OT_TRIANGLE_LIST);

	plane->position(0,0,0);
	plane->colour(cv);

	plane->position(0,0,500);
	plane->colour(cv);

	plane->position(500,0,500);
	plane->colour(cv);

	plane->position(500,0,0);
	plane->colour(cv);

	plane->quad(0,1,2,3);

	plane->end();
	std::stringstream ss, ss2;
	ss << "MyPlane" << material;
	ss2 << "customplane" << material << ".mesh";
	Ogre::MeshPtr ptr = plane->convertToMesh(ss.str().c_str());
	Ogre::MeshSerializer ser;
	ser.exportMesh(ptr.getPointer(), ss2.str().c_str());

	return plane;


}

//-------------------------------------------------------------------------------------
Ogre::ManualObject* const RenderingTest::cube (bool isFrustum) // cube with 24 vertices for the UV mapping
{
	Ogre::ColourValue cv;
	cv.r =0.05; cv.g = 0.02; cv.b = 0.93; cv.a = 0.85;
	

	char* name = (char*) malloc (32);
	sprintf(name, "cube%d", cubeCount++);
	Ogre::ManualObject* cube = mSceneMgr->createManualObject(name);

	cube->begin("UVMappingMaterial",Ogre::RenderOperation::OT_TRIANGLE_LIST);
	
	cube->position(-1,-1,1);
    cube->normal(0,0,1);
    cube->textureCoord(0,1);
	cube->colour(cv);

    cube->position(1,-1,1);
    cube->normal(0,0,1);
    cube->textureCoord(1,1);
	cube->colour(cv);

    cube->position(1,1,1);
    cube->normal(0,0,1);
    cube->textureCoord(1,0);
	cube->colour(cv);

    cube->position(-1,1,1);
    cube->normal(0,0,1);
    cube->textureCoord(0,0);
	cube->colour(cv);

    cube->quad(0,1,2,3);

    cube->position(1,-1,-1);
    cube->normal(0,0,-1);
    cube->textureCoord(0,1);
	cube->colour(cv);

    cube->position(-1,-1,-1);
    cube->normal(0,0,-1);
    cube->textureCoord(1,1);
	cube->colour(cv);

    cube->position(-1,1,-1);
    cube->normal(0,0,-1);
    cube->textureCoord(1,0);
	cube->colour(cv);

    cube->position(1,1,-1);
    cube->normal(0,0,-1);
    cube->textureCoord(0,0);
	cube->colour(cv);

    cube->quad(4,5,6,7);

    cube->position(1,-1,1);
    cube->normal(1,0,0);
    cube->textureCoord(0,1);
	cube->colour(cv);

    cube->position(1,-1,-1);
    cube->normal(1,0,0);
    cube->textureCoord(1,1);
	cube->colour(cv);

    cube->position(1,1,-1);
    cube->normal(1,0,0);
    cube->textureCoord(1,0);
	cube->colour(cv);

    cube->position(1,1,1);
    cube->normal(1,0,0);
    cube->textureCoord(0,0);
	cube->colour(cv);

    cube->quad(8,9,10,11);

    cube->position(-1,-1,-1);
    cube->normal(-1,0,0);
    cube->textureCoord(0,1);
	cube->colour(cv);

    cube->position(-1,-1,1);
    cube->normal(-1,0,0);
    cube->textureCoord(1,1);
	cube->colour(cv);

    cube->position(-1,1,1);
    cube->normal(-1,0,0);
    cube->textureCoord(1,0);
	cube->colour(cv);

    cube->position(-1,1,-1);
    cube->normal(-1,0,0);
    cube->textureCoord(0,0);
	cube->colour(cv);

    cube->quad(12,13,14,15);

    cube->position(-1,1,1);
    cube->normal(0,1,0);
    cube->textureCoord(0,1);
	cube->colour(cv);

    cube->position(1,1,1);
    cube->normal(0,1,0);
    cube->textureCoord(1,1);
	cube->colour(cv);

    cube->position(1,1,-1);
    cube->normal(0,1,0);
    cube->textureCoord(1,0);
	cube->colour(cv);

    cube->position(-1,1,-1);
    cube->normal(0,1,0);
    cube->textureCoord(0,0);
	cube->colour(cv);

    cube->quad(16,17,18,19);

    cube->position(-1,-1,-1);
    cube->normal(0,-1,0);
    cube->textureCoord(0,1);
	cube->colour(cv);

    cube->position(1,-1,-1);
    cube->normal(0,-1,0);
    cube->textureCoord(1,1);
	cube->colour(cv);

    cube->position(1,-1,1);
    cube->normal(0,-1,0);
    cube->textureCoord(1,0);
	cube->colour(cv);

    cube->position(-1,-1,1);
    cube->normal(0,-1,0);
    cube->textureCoord(0,0);
	cube->colour(cv);

    cube->quad(20,21,22,23);

       
	
	cube->end();

	Ogre::MeshPtr ptr = cube->convertToMesh("MyCube");
	Ogre::MeshSerializer ser;
	ser.exportMesh(ptr.getPointer(), "customcube.mesh");
	free (name);

	return cube;


}
//---------------------------------------------------------------------------------------
std::string RenderingTest::parseDateInput(const char* date)
{
	char *day, *month, *year;
	char *value = (char*) malloc (strlen (date) +2 );
	sprintf(value, "%s", date);
	char *dummy;
	dummy = strtok (value, "-"); // day
	day = dummy;
	dummy = strtok (NULL, "-"); // month
	month = dummy;
	dummy = strtok (NULL, " "); // year
	year = dummy;

	std::stringstream parsedDate;
	parsedDate << year << month << day;

	free(value);
	day = month = year = value = NULL;

	return parsedDate.str();
}

//---------------------------------------------------------------------------------------
const char RenderingTest::selectedRb()
{
	return (GUI::getInstance()->rb1->isSelected() ? 'a' : GUI::getInstance()->rb2->isSelected() ? 'm' : 'c');
}

//---------------------------------------------------------------------------------------
const char* const RenderingTest::orderIn()
{
	return (GUI::getInstance()->tb->isSelected() ? "desc" : " ");
}

//---------------------------------------------------------------------------------------
const std::string RenderingTest::buildQuery ()
{
	std::stringstream queryBuilder;

	queryBuilder << "SELECT * FROM file where ( " << selectedRb() << " > " << parseDateInput(GUI::getInstance()->d1->getText().c_str()) << " and  " << selectedRb() << " < " 
		<< parseDateInput(GUI::getInstance()->d2->getText().c_str()) << " ) order by " << selectedRb() << " " << orderIn() << " ;" ; 

	return queryBuilder.str();
}
//---------------------------------------------------------------------------------------
bool RenderingTest::rb1StateChanged(const CEGUI::EventArgs &e)
{
	if (GUI::getInstance()->rb1->isSelected())
		GUI::getInstance()->tt1->setText("Accessed files between: ");
	return true;
}

//---------------------------------------------------------------------------------------
bool RenderingTest::rb2StateChanged(const CEGUI::EventArgs &e)
{
	if (GUI::getInstance()->rb2->isSelected())
		GUI::getInstance()->tt1->setText("Modified files between: ");
	return true;
}

//---------------------------------------------------------------------------------------
bool RenderingTest::rb3StateChanged(const CEGUI::EventArgs &e)
{
	if (GUI::getInstance()->rb3->isSelected())
		GUI::getInstance()->tt1->setText("Created files between: ");
	return true;
}

bool RenderingTest::visualise(const CEGUI::EventArgs &e)
{
	OutputDebugString("pressed\nn\nn\nn");
	drawObjects(3000);
	

	return true;
}

void RenderingTest::drawObjects(unsigned amount)
{
	
	if (rendered)
	{
		try
		{

			ancestorCube->removeAndDestroyAllChildren();
			ancestorPlane->removeAndDestroyAllChildren();
			mSceneMgr->destroyAllMovableObjects(); //does not destroy movable texts, they are needed to be destroyed manually
			//mSceneMgr->destroyAllManualObjects();
			rectangleCount = cubeCount = planeCount = 0;

			for (int i = 0; i < 15000; ++i)
			{
				cubes[i] = NULL;
				planes[i] = NULL;
				textureNodes[i] = NULL;
			}
			Ogre::Entity* palm = mSceneMgr->createEntity("palm","sphere.mesh");
			palmNode->attachObject(palm);
			palmNode->scale(.25,.1,.5);
			palmNode->setVisible(false);
			Ogre::Entity* bone;
			char str[3];
			for (int i = 0; i < 20; ++i)
			{
				sprintf(str, "%d", i);
				bone = mSceneMgr->createEntity(str,"sphere.mesh");
				bonesArr[i]->attachObject(bone);
				
			}
			Ogre::MaterialPtr redMaterial = Ogre::MaterialManager::getSingleton().load( "FullRed", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
			Ogre::Entity* knot = mSceneMgr->createEntity("Knot", "knot.mesh");
			knot->setMaterial(redMaterial);
			knotNode->attachObject(knot);


		}
		catch(std::exception e)
		{
			OutputDebugStringA(e.what());
		}
		
		rendered = false;
	}
	



	planeCount = 0;
	planeLength = 1;
	unsigned k = amount;
	std::string objName = "cube";
	std::string planeName = "plane";
	std::stringstream ss;
	bool isCube;
	int direction;
	Ogre::Vector3 position (0,0,0);
	Ogre::Vector3 planePosition (-125,0,-125);
	srand (time(NULL));
		
	MYSQL_RES *res_set; /* Create a pointer to recieve the return value.*/
	MYSQL_ROW row; /* Assign variable for rows. */
	

	try
	{
		OutputDebugString("\n\n\nn\nn\nn\nn\nn\nn\nn\nn\nn\n\n\nn\n");
		OutputDebugString(buildQuery().c_str());
		res_set = mysqlExecute(buildQuery().c_str());		
		unsigned long long int numrows = mysql_num_rows(res_set); /* Create the count to print all rows */
		if (numrows == 0)
			goto end;
	

		for (int j = 0; j < 500000; j++)
		{
		

			direction = (j%2 == 0) ? 1 : -1;
			position.x = (direction == -1) ? 12000 : 0;
			position.y = 0;
			
			if (planeLength != 1)
			{
						
				planes[planeCount-1]->scale(planeLength,1,1);
				planeLength = 1;
			}
			previousPlaneDate = 111111111; // infeasible value
				
			for (int i = 0; i < 25; i++)
			{
				
				row = mysql_fetch_row(res_set);

				app.e->name = row[0];
				app.e->directory = row[1];
				app.e->size = std::stoull(row[2]); //for unsigned long long
					
				app.e->setExtension();
				std::string ext = app.e->extension;
				
				

				for (unsigned int i = 0; i < ext.length(); ++i)
				{
					ext[i] = std::toupper(ext[i]);
				}

				app.e->extension = ext;

				app.e->write_permission = std::stoi(row[4]);
				app.e->access_permission = std::stoi(row[5]);
				app.e->creation_time = row[6];
				app.e->access_time = row[7];
				app.e->modification_time = row[8];
				app.e->c = std::stoi(row[9]);
				app.e->a = std::stoi(row[10]);
				app.e->m = std::stoi(row[11]);
			
				OutputDebugStringA(app.e->extension.c_str());


			



				float height = (((int) (app.e->size /(float) 100.0 + 50)) % 2500); //height of the cuboids
				int boy =  50 * (1 + rand()  % 20);
				float fractions = (rand() % 1000) / 1000;

				if (k <= 0)
					goto end;
				isCube = (i%2 == 0);
				ss << objName << i << " " << j;
				Ogre::Entity* object = mSceneMgr->createEntity(ss.str().c_str(), "MyCube");

				std::string materialName;

				if(app.e->write_permission == 0)
				{
					if (app.e->access_permission == 0)
						materialName = "ReadOnly";
					else
						materialName = "ReadExecute";
				}
				else if (app.e->write_permission == 1)
				{
					if (app.e->access_permission == 0)
						materialName = "ReadWrite";
					else
						materialName = "ReadWriteExecute";
				}
			
				Ogre::MaterialPtr ptr = Ogre::MaterialManager::getSingleton().load( materialName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
				Ogre::MaterialPtr ptr2 = ptr->clone("cloneMat");
				//ptr2->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(2.0,.0);
				ptr2->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureScale(1.0,50 / height);
			/*	ptr2->getTechnique(0)->getPass(0)->setDiffuse((boy % 17) / 17.0 - fractions, (boy % 63) / 63.0 - fractions, (boy % 61) / 61.0 - fractions, 0.8);
				ptr2->getTechnique(0)->getPass(0)->setSpecular((boy % 6) / 6.0 - fractions, (boy % 66) / 66.0 - fractions, (boy % 55) / 55.0 - fractions, 0.8);
				*/
				std::stringstream ss2;
				if (app.e->extension == "")
					app.e->extension = "...";
				else if (app.e->extension.length() == 2)
				{
					ss2 << app.e->extension << '.';
					app.e->extension = ss2.str();
				}
				else if (app.e->extension.length() == 1)
				{
					ss2 << app.e->extension << "..";
					app.e->extension = ss2.str();
				}

				ptr2->getTechnique(0)->getPass(0)->setAmbient((1/16.0 * ((app.e->extension[0] - '!') % 16 + 1)),(1/16.0 * ((app.e->extension[1] - '!') % 16 + 1)),(1/16.0 * ((app.e->extension[2] - '!') % 16 + 1)));
				object->setMaterial(ptr2);
			
				std::ostringstream oss;
				oss << "text" << textCount;

				textNodes[textCount] = ancestorCube->createChildSceneNode();
				cubes[cubeCount] = ancestorCube->createChildSceneNode();
				cubes[cubeCount]->attachObject(object);
				Ogre::MovableText* text = new Ogre::MovableText((oss.str().c_str()),app.e->name);
				text->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_CENTER); // Center horizontally and display above the node	
				text->setColor(Ogre::ColourValue(1,1,1,1));
				text->setCharacterHeight(48.0);
				textNodes[textCount]->attachObject(text);
				//textNodes[textCount]->setScale(50,50,50);

				if (app.e->isTextureFile())
				{
					try
					{
					
						char textureName[50];
						sprintf(textureName,"texture_%d",textureCount++);
						std::stringstream ss;
						ss << app.e->directory << "/" << app.e->name;
						//loads and adds the image to the resources/////
						
						Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName("MyMaterial2");
						Ogre::MaterialPtr matClone = material->clone(textureName);
						loadImageFile(textureName,ss.str());
						matClone->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureName(textureName);
						
						textureNodes[textureCount] = cubes[cubeCount]->createChildSceneNode(textureName);
						textureNodes[textureCount]->attachObject(rectangle(textureName));
						textureNodes[textureCount]->setPosition(0,0,0);
						textureNodes[textureCount]->scale(.02,.01,.02);
						textureNodes[textureCount]->translate(-1,0,-1);
					}
					catch(std::exception e)
					{
						
						OutputDebugStringA("\nm\nm\nm\nm\nm\nm\nm\nm");
						OutputDebugStringA(e.what());
					};


					


				}



				std::stringstream ss1;
					
				if (planeCount == 0) // first plane 
				{
					planeContainers[planeCount] = ancestorPlane->createChildSceneNode();
					planes[planeCount] = planeContainers[planeCount]->createChildSceneNode();
					planeText[planeCount] = planeContainers[planeCount]->createChildSceneNode();
					ss1 << "plane" << i << j;
					Ogre::Entity* object = mSceneMgr->createEntity(ss1.str().c_str(), goldenPlane);
					//planePointer->detachObject(ss1.str().c_str());
					planes[planeCount]->attachObject(object);
					planeContainers[planeCount]->setPosition(position);
					planes[planeCount]->translate(-250,-50,-250);
					
					
					Ogre::MovableText* text = new Ogre::MovableText((oss.str().c_str()),GUI::getInstance()->rb1->isSelected() ? app.e->access_time : GUI::getInstance()->rb2->isSelected() ? app.e->modification_time : app.e->creation_time);
					text->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_CENTER); // Center horizontally and display above the node	
					text->setColor(Ogre::ColourValue(1,1,1,1));
					text->setCharacterHeight(48.0);
					planeText[planeCount]->attachObject(text);
					planeText[planeCount]->translate(0,0,-150);
					
					//planeContainers[planeCount]->setPosition(planes[planeCount]->getPosition());
					//planePointer->scale(3,1,2);
					isGoldenPlane = true;
					previousPlaneDate = app.e->a;
					planeCount++;
					//planePointer->translate(250,0,0);
				}
				else
				{
					if (app.e->a == previousPlaneDate)
					{
						++planeLength;
						
					}
					else
					{
						//planePointer->scale(planeLength*3,1,1);
						planeContainers[planeCount] = ancestorPlane->createChildSceneNode();
						planes[planeCount] = planeContainers[planeCount]->createChildSceneNode();
						planeText[planeCount] = planeContainers[planeCount]->createChildSceneNode();
						ss1 << "plane" << i << " " << j;
						Ogre::Entity* object = mSceneMgr->createEntity(ss1.str().c_str(), (planeCount % 2 == 0) ? goldenPlane : otherPlane);
						planes[planeCount]->attachObject(object);
						planeContainers[planeCount]->setPosition(position);
						planes[planeCount]->translate(-250,-50,-250);
						Ogre::MovableText* text = new Ogre::MovableText((oss.str().c_str()),GUI::getInstance()->rb1->isSelected() ? app.e->access_time : GUI::getInstance()->rb2->isSelected() ? app.e->modification_time : app.e->creation_time);
						text->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_CENTER); // Center horizontally and display above the node	
						text->setColor(Ogre::ColourValue(1,1,1,1));
						text->setCharacterHeight(48.0);
						planeText[planeCount]->attachObject(text);
						planeText[planeCount]->translate(0,0,-150);
						//planeContainers[planeCount]->setPosition(planes[planeCount]->getPosition());
						if (direction == -1)
						{
							planes[planeCount]->yaw((Ogre::Radian)Ogre::Math::PI);
							planes[planeCount]->translate(500,0,500);
						}
					
						if (planeLength != 1)
						{
							planes[planeCount-1]->scale(planeLength,1,1);
							planeLength = 1;
						}
						previousPlaneDate = app.e->a;
						//planes[planeCount]->translate(0,i*75,0);
						planeCount++;
						

						
					}

				}
				
			

				cubes[cubeCount]->scale(50,height,50);
				position.y += height;
				textNodes[textCount]->setPosition (position);
				cubes[cubeCount]->setPosition (position);
				textNodes[textCount]->translate(0,height + 20,0);
				position.x += 500 * direction;
				k--;
				position.y = 0;
				++textCount;
			}

			position.z += 500;
			
				

		}
end:
		rendered = true;
		OutputDebugStringA("end");

		
	}
	catch (std::exception& e)
	{
		OutputDebugStringA("\nn\n\nn\nn\nn\nn\nnEXCEPTIONNNNN\NN\NN\NN\nn\nn\nn\nn\nn\nn");
		OutputDebugStringA(e.what());
	}



}

bool RenderingTest::go(void)
{
#ifdef _DEBUG
    mResourcesCfg = "resources_d.cfg";
    mPluginsCfg = "plugins_d.cfg";
#else
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";
#endif

    // construct Ogre::Root
    mRoot = new Ogre::Root(mPluginsCfg);

//-------------------------------------------------------------------------------------
    // setup resources
    // Load resource paths from config file
    Ogre::ConfigFile cf;
    cf.load(mResourcesCfg);

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
                archName, typeName, secName);
        }
    }
//-------------------------------------------------------------------------------------
    // configure
    // Show the configuration dialog and initialise the system
    // You can skip this and use root.restoreConfig() to load configuration
    // settings if you were sure there are valid ones saved in ogre.cfg
    if(mRoot->showConfigDialog())
    {
        // If returned true, user clicked OK so initialise
        // Here we choose to let the system create a default rendering window by passing 'true'
        mWindow = mRoot->initialise(true, "RenderingTest Render Window");

        // Let's add a nice window icon
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
        HWND hwnd;
        mWindow->getCustomAttribute("WINDOW", (void*)&hwnd);
        LONG iconID   = (LONG)LoadIcon( GetModuleHandle(0), MAKEINTRESOURCE(IDI_APPICON) );
        SetClassLong( hwnd, GCL_HICON, iconID );
#endif
    }
    else
    {
        return false;
    }
//-------------------------------------------------------------------------------------
    // choose scenemanager
    // Get the SceneManager, in this case a generic one
    mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC);
//-------------------------------------------------------------------------------------
    // create camera
    // Create the camera
    mCamera = mSceneMgr->createCamera("PlayerCam");

    // Position it at 500 in Z direction
    mCamera->setPosition(Ogre::Vector3(3227,5300,8700));
    // Look back along -Z
    mCamera->lookAt(Ogre::Vector3(3227,1500,4000));
    //mCamera->setNearClipDistance(0.01);

    mCameraMan = new OgreBites::SdkCameraMan(mCamera);   // create a default camera controller
//-------------------------------------------------------------------------------------
    // create viewports
    // Create one viewport, entire window
    Ogre::Viewport* vp = mWindow->addViewport(mCamera);
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

	

    // Alter the camera aspect ratio to match the viewport
    mCamera->setAspectRatio(
        Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
//-------------------------------------------------------------------------------------
    // Set default mipmap level (NB some APIs ignore this)
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
//-------------------------------------------------------------------------------------
    // Create any resource listeners (for loading screens)
    //createResourceListener();
//-------------------------------------------------------------------------------------
    // load resources
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
//-------------------------------------------------------------------------------------

	pinchFlag = false;

	cube(false);
	plane("Ogre/Earring");
	plane("Ogre/Earring2");
	//scanHardDrive("C:/");
	myfile.close();
	mysqlConn();
			
/*	mysqlExecute("LOAD DATA LOCAL INFILE 'C:\\\\Users\\\\tunce_000\\\\Documents\\\\Visual Studio 2010\\\\Projects\\\\RenderingTest\\\\RenderingTest\\\\res\\\\bulk.txt' INTO TABLE `test`.`seniorprojectfiles` FIELDS TERMINATED BY \'<\' LINES TERMINATED BY \'>\'");	*/	




	// leap controller and listener objects
	leapController.addListener(leapSampleListener);
	leapController.setPolicyFlags(Leap::Controller::POLICY_BACKGROUND_FRAMES);
	leapSampleListener.onFrame(leapController);
	// end leap
	//mSceneMgr->setAmbientLight(Ogre::ColourValue(0.25, 0.25, 0.25));
	//hand
	handNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("HandNode");
	knotNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("KnotNode");
	Ogre::Entity* knot = mSceneMgr->createEntity("Knot", "knot.mesh");
	Ogre::MaterialPtr redMaterial = Ogre::MaterialManager::getSingleton().load( "FullRed", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
	knot->setMaterial(redMaterial);
	knotNode->attachObject(knot);
	knotNode->setScale(.1,.1,.1);
	knotNode->setVisible(false);
	//handNode->setPosition (0, -300, -500);
	// palm
	palmNode = handNode->createChildSceneNode("PalmNode");
	Ogre::Entity* palm = mSceneMgr->createEntity("palm","sphere.mesh");
	palmNode->attachObject(palm);
	palmNode->scale(.25,.1,.5);
	palmNode->setVisible(false);
	// fingers
	//Ogre::Entity* fingers = new Ogre::Entity[5];
	fingersNode = handNode->createChildSceneNode("FingersNode");
	Ogre::Entity* bone;
	char str[3];
	for (int i = 0; i < 20; ++i)
	{
		sprintf(str, "%d", i);
		bone = mSceneMgr->createEntity(str,"sphere.mesh");
		bonesArr[i] = fingersNode->createChildSceneNode(str);
		bonesArr[i]->attachObject(bone);
		bonesArr[i]->scale(.1,.1,.1);
	}


	
	// add points
	linePoints.push_back(Ogre::Vector3(0.0f, 0.0f, 0.0f ));
	linePoints.push_back(Ogre::Vector3(452.0f, 2345.0f, 453.0f));
 
	
	DynamicLines *lines = new DynamicLines(Ogre::RenderOperation::OT_LINE_LIST);
	for (int i=0; i<linePoints.size(); i++) {
	  lines->addPoint(linePoints[i]);
	}
	lines->update();
	Ogre::SceneNode *linesNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("lines");
	linesNode->attachObject(lines);
	linesNode->setVisible(false);


	ancestorCube = mSceneMgr->getRootSceneNode()->createChildSceneNode("AncestorCube");
	ancestorPlane = mSceneMgr->getRootSceneNode()->createChildSceneNode("AncestorPlane");
	

	initGUI();

	GUI::getInstance()->RenderButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&RenderingTest::visualise, this));

	

	

	
    // Set ambient light
    //mSceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));

    // Create a light
    //Ogre::Light* l = mSceneMgr->createLight("MainLight");
    //l->setPosition(20,80,50);
//-------------------------------------------------------------------------------------
    //create FrameListener
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    mWindow->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    mInputManager = OIS::InputManager::createInputSystem( pl );

    mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject( OIS::OISKeyboard, true ));
    mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject( OIS::OISMouse, true ));

    mMouse->setEventCallback(this);
    mKeyboard->setEventCallback(this);

    //Set initial mouse clipping size
    windowResized(mWindow);

    //Register as a Window listener
    Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);

    mTrayMgr = new OgreBites::SdkTrayManager("InterfaceName", mWindow, mMouse, this);
	mTrayMgr->hideLogo();
	mTrayMgr->hideFrameStats();
    mTrayMgr->hideCursor();

    // create a params panel for displaying sample details
    Ogre::StringVector items;
    items.push_back("cam.pX");
    items.push_back("cam.pY");
    items.push_back("cam.pZ");
    items.push_back("");
    items.push_back("cam.oW");
    items.push_back("cam.oX");
    items.push_back("cam.oY");
    items.push_back("cam.oZ");
    items.push_back("");
    items.push_back("Filtering");
    items.push_back("Poly Mode");

    mDetailsPanel = mTrayMgr->createParamsPanel(OgreBites::TL_NONE, "DetailsPanel", 200, items);
    mDetailsPanel->setParamValue(9, "Bilinear");
    mDetailsPanel->setParamValue(10, "Solid");
    mDetailsPanel->hide();

    mRoot->addFrameListener(this);
//-------------------------------------------------------------------------------------
    mRoot->startRendering();

    return true;
}





//-------------------------------------------------------------------------------------
bool RenderingTest::updateFrame(const Ogre::FrameEvent& evt)
{
	//leap
	static Frame frame;
	frame = leapController.frame();
	Leap::Hand rightMost = frame.hands().rightmost();
	previousFramePitch = rightMost.direction().pitch() * RAD_TO_DEG;
	previousFrameYaw = rightMost.direction().yaw() * RAD_TO_DEG;
	previousFrameRoll = rightMost.palmNormal().roll() * RAD_TO_DEG;


	Ogre::SceneNode *lnode = dynamic_cast<Ogre::SceneNode*>(mSceneMgr->getRootSceneNode()->getChild("lines"));
	DynamicLines *lines = dynamic_cast<DynamicLines*>(lnode->getAttachedObject(0));
 
	// add points if you wish (maybe elsewhere, this is just an example)
	linePoints.pop_front();
	linePoints.push_front(bonesArr[7]->getPosition());
 
	if (lines->getNumPoints()!= linePoints.size()) {
	  // Oh no!  Size changed, just recreate the list from scratch
	  lines->clear();
	  for (int i=0; i<linePoints.size(); ++i) {
		lines->addPoint(linePoints[i]);
	  }
	}
	else {
	  // Just values have changed, use 'setPoint' instead of 'addPoint'
	  for (int i=0; i<linePoints.size(); ++i) {
		lines->setPoint(i,linePoints[i]);
	  }
	}
	lines->update();


	if (!frame.hands().isEmpty() && !handOrientationFlag)
	{	
		palmNode->resetOrientation();
		handOrientationFlag = true;
	}
	else if (handOrientationFlag && frame.hands().isEmpty() )
	{
		handOrientationFlag = false;
	}

	if (rightMost.pinchStrength() >= (float) 0.75 && pinchFlag)
	{
		scrollPosition = toVector(rightMost.fingers().fingerType(Leap::Finger::TYPE_INDEX)[0].tipPosition());
		knotNode->setVisible(true);
		lnode->setVisible(true);
		knotNode->setPosition(bonesArr[7]->getPosition());
		pinchFlag = false;
		linePoints.pop_back();
		linePoints.push_back(knotNode->getPosition());
	}

	else if (rightMost.pinchStrength() <= (float) 0.3)
	{
		pinchFlag = true;
		knotNode->setVisible(false);
		lnode->setVisible(false);
	}




	if (!frame.hands().isEmpty())
	{
		Leap::Hand rightMost = frame.hands().rightmost();
		palmNode->setPosition(mCamera->getPosition().x, mCamera->getPosition().y - 500, mCamera->getPosition().z - 500); // between 100 and 250
		mCamera->lookAt(palmNode->getPosition());
		

		float pitchValue = rightMost.direction().pitch() * RAD_TO_DEG;
		palmNode->pitch((Ogre::Radian) (pitchValue - previousFramePitch) );
		float rollValue = rightMost.palmNormal().roll() * RAD_TO_DEG;
		palmNode->roll((Ogre::Radian) (rollValue - previousFrameRoll) );
		float yawValue = rightMost.direction().yaw() * RAD_TO_DEG;
		palmNode->yaw((Ogre::Radian) (yawValue - previousFrameYaw) );
		/*char* dummy = (char*) malloc(64);
		sprintf (dummy, "pitch: %f, yaw: %f, roll: %f\n", pitchValue, yawValue, rollValue);
		OutputDebugString (dummy);
		free(dummy);*/
		previousFramePitch = rightMost.direction().pitch() * RAD_TO_DEG;
		previousFrameYaw = rightMost.direction().yaw() * RAD_TO_DEG;
		previousFrameRoll = rightMost.palmNormal().roll() * RAD_TO_DEG;
		// Get fingers
		static FingerList fingers;
		fingers = rightMost.fingers();
		int i = 0;
		int index = 0; //between 0 and 19 (finger bones)
		for (FingerList::const_iterator fl = fingers.begin(); fl != fingers.end(); ++fl)
		{
			static Finger finger;
			finger = *fl;
			/*char* dummy = (char*) malloc(128);
			sprintf (dummy, "finger id: %d, length: %f, width: %f\n", finger.id(), finger.length(), finger.width());
			OutputDebugString (dummy);
			free(dummy);*/
			// Get finger bones
			for (int b = 0; b < 4; ++b, ++index)
			{
				static Bone::Type boneType;
				boneType = static_cast<Bone::Type>(b);
				static Bone bone;
				bone = finger.bone(boneType);
				bonesArr[i++]->setPosition(palmNode->getPosition().x + bone.center().x, palmNode->getPosition().y + bone.center().y, palmNode->getPosition().z + bone.center().z);
			}
		}
		if (rightMost.pinchStrength() > (float) 0.75)
			mCamera->setPosition(mCamera->getPosition() + ((toVector(rightMost.fingers().fingerType(Leap::Finger::TYPE_INDEX)[0].tipPosition()) - scrollPosition) * (toVector(rightMost.fingers().fingerType(Leap::Finger::TYPE_INDEX)[0].tipPosition()) - scrollPosition) * (toVector(rightMost.fingers().fingerType(Leap::Finger::TYPE_INDEX)[0].tipPosition()) - scrollPosition)) / 125000);

	}
	std::stringstream ss;
	ss << "x: " << mCamera->getPosition().x << "y: " << mCamera->getPosition().y << "z: " << mCamera->getPosition().z << " \n ";
	//OutputDebugStringA(ss.str().c_str());

	return true;
}


void RenderingTest::initGUI()
{
	GUI::getInstance();
	GUI::getInstance()->rb1->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged, CEGUI::Event::Subscriber(&RenderingTest::rb1StateChanged, this));
	GUI::getInstance()->rb2->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged, CEGUI::Event::Subscriber(&RenderingTest::rb2StateChanged, this));
	GUI::getInstance()->rb3->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged, CEGUI::Event::Subscriber(&RenderingTest::rb3StateChanged, this));
}

bool RenderingTest::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	Ogre::RenderTarget::FrameStats stats = mWindow->getStatistics();
	std::stringstream ss;
	ss << "FPS: " << (int) stats.lastFPS << "\n# of Triangles: " << stats.triangleCount;

	GUI::getInstance()->FPSLabel->setText(ss.str().c_str());
	

    if(mWindow->isClosed())
        return false;

    if(mShutDown)
        return false;

    //Need to capture/update each device
    mKeyboard->capture();
    mMouse->capture();

    mTrayMgr->frameRenderingQueued(evt);

    if (!mTrayMgr->isDialogVisible())
    {
        mCameraMan->frameRenderingQueued(evt);   // if dialog isn't up, then update the camera
        if (mDetailsPanel->isVisible())   // if details panel is visible, then update its contents
        {
            mDetailsPanel->setParamValue(0, Ogre::StringConverter::toString(mCamera->getDerivedPosition().x));
            mDetailsPanel->setParamValue(1, Ogre::StringConverter::toString(mCamera->getDerivedPosition().y));
            mDetailsPanel->setParamValue(2, Ogre::StringConverter::toString(mCamera->getDerivedPosition().z));
            mDetailsPanel->setParamValue(4, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().w));
            mDetailsPanel->setParamValue(5, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().x));
            mDetailsPanel->setParamValue(6, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().y));
            mDetailsPanel->setParamValue(7, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().z));
        }
    }

	updateFrame(evt);

	//Need to inject timestamps to CEGUI System.
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);

    return true;
}
//-------------------------------------------------------------------------------------
bool RenderingTest::keyPressed( const OIS::KeyEvent &arg )
{
    if (mTrayMgr->isDialogVisible()) return true;   // don't process any more keys if dialog is up

    if (arg.key == OIS::KC_F)   // toggle visibility of advanced frame stats
    {
       // mTrayMgr->toggleAdvancedFrameStats();
    }
    else if (arg.key == OIS::KC_G)   // toggle visibility of even rarer debugging details
    {

    }

	else if (arg.key == OIS::KC_H)
	{
		GUI::showHideGUI();
	}

    else if (arg.key == OIS::KC_T)   // cycle polygon rendering mode
    {

    }
    else if (arg.key == OIS::KC_R)   // cycle polygon rendering mode
    {
        Ogre::String newVal;
        Ogre::PolygonMode pm;

        switch (mCamera->getPolygonMode())
        {
        case Ogre::PM_SOLID:
            newVal = "Wireframe";
            pm = Ogre::PM_WIREFRAME;
            break;
        case Ogre::PM_WIREFRAME:
            newVal = "Points";
            pm = Ogre::PM_POINTS;
            break;
        default:
            newVal = "Solid";
            pm = Ogre::PM_SOLID;
        }

        mCamera->setPolygonMode(pm);
        mDetailsPanel->setParamValue(10, newVal);
    }

	else if(arg.key == OIS::KC_B)
		mCamera->setPosition(Ogre::Vector3(3227,5300,8700));

    else if(arg.key == OIS::KC_F5)   // refresh all textures
    {
        Ogre::TextureManager::getSingleton().reloadAll();
    }
    else if (arg.key == OIS::KC_SYSRQ)   // take a screenshot
    {
        mWindow->writeContentsToTimestampedFile("screenshot", ".jpg");
    }
    else if (arg.key == OIS::KC_ESCAPE)
    {
        mShutDown = true;
    }

	CEGUI::System &sys = CEGUI::System::getSingleton();
	sys.getDefaultGUIContext().injectKeyDown((CEGUI::Key::Scan) arg.key);
	sys.getDefaultGUIContext().injectChar(arg.text);
    mCameraMan->injectKeyDown(arg);

    return true;
}

bool RenderingTest::keyReleased( const OIS::KeyEvent &arg )
{
	CEGUI::System &sys = CEGUI::System::getSingleton();
	sys.getDefaultGUIContext().injectKeyUp((CEGUI::Key::Scan) arg.key);
    mCameraMan->injectKeyUp(arg);
    return true;
}

bool RenderingTest::mouseMoved( const OIS::MouseEvent &arg )
{
	mCameraMan->injectMouseMove(arg);

    CEGUI::System &sys = CEGUI::System::getSingleton();
	sys.getDefaultGUIContext().injectMouseMove(arg.state.X.rel, arg.state.Y.rel);
	// Scroll wheel.
	if (arg.state.Z.rel)
		sys.getDefaultGUIContext().injectMouseWheelChange(arg.state.Z.rel / 120.0f);
    return true;
}

bool RenderingTest::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
    if (mTrayMgr->injectMouseDown(arg, id)) return true;
    mCameraMan->injectMouseDown(arg, id);
    return true;
}

bool RenderingTest::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
	CEGUI::System &sys = CEGUI::System::getSingleton();
	sys.getDefaultGUIContext().injectMouseButtonDown(convertButton(id));
	sys.getDefaultGUIContext().injectMouseButtonUp(convertButton(id));
    mCameraMan->injectMouseUp(arg, id);
    return true;
}

//Adjust mouse clipping area
void RenderingTest::windowResized(Ogre::RenderWindow* rw)
{
    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState &ms = mMouse->getMouseState();
    ms.width = width;
    ms.height = height;
}

//Unattach OIS before window shutdown (very important under Linux)
void RenderingTest::windowClosed(Ogre::RenderWindow* rw)
{
    //Only close for window that created OIS (the main window in these demos)
    if( rw == mWindow )
    {
        if( mInputManager )
        {
            mInputManager->destroyInputObject( mMouse );
            mInputManager->destroyInputObject( mKeyboard );

            OIS::InputManager::destroyInputSystem(mInputManager);
            mInputManager = 0;
        }
    }
}



#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        RenderingTest app;

        try {
            app.go();
        } catch( Ogre::Exception& e ) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occured: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif
