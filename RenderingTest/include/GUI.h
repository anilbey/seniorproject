#include <iostream>
//CEGUI
#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>


//Singleton Pattern
class GUI
{

private:

	
	static GUI* instance;
	CEGUI::OgreRenderer* mRenderer;
	~GUI()
	{
		delete instance;
	}
	GUI()
	{
			
		mRenderer = &CEGUI::OgreRenderer::bootstrapSystem();

		CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
		CEGUI::Font::setDefaultResourceGroup("Fonts");
		CEGUI::Scheme::setDefaultResourceGroup("Schemes");
		CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
		CEGUI::WindowManager::setDefaultResourceGroup("Layouts");	

		CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
		CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");
		CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setVisible( true );

		CEGUI::WindowManager &wmgr = CEGUI::WindowManager::getSingleton();
		this->sheet = wmgr.createWindow("DefaultWindow", "CEGUIDemo/Sheet");
		
		this->w2 = wmgr.createWindow("DefaultWindow", "CEGUIDemo/Sheet");
		w2->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.075,0)));


		this->label1 = static_cast<CEGUI::Tooltip*> (wmgr.createWindow("AlfiskoSkin/Button","Label1"));
		this->label1->setPosition(CEGUI::UVector2(CEGUI::UDim(-0.05,0),CEGUI::UDim(0.075,0)));
		this->label1->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->label1->setText("Directory Selection");
		this->sheet->addChild(this->label1);


		this->FPSLabel = static_cast<CEGUI::Tooltip*> (wmgr.createWindow("AlfiskoSkin/Button", "FPSLabel"));
		this->FPSLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.93,0)));
		this->FPSLabel->setSize(CEGUI::USize(CEGUI::UDim(0.16, 0), CEGUI::UDim(0.07, 0)));
		this->FPSLabel->setText("FPS: ");
		this->sheet->addChild(this->FPSLabel);

		this->label2 = static_cast<CEGUI::Tooltip*> (wmgr.createWindow("AlfiskoSkin/Button","Label2"));
		this->label2->setPosition(CEGUI::UVector2(CEGUI::UDim(-0.05,0),CEGUI::UDim(0,0)));
		this->label2->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->label2->setText("File Set Selection");
		this->w2->addChild(this->label2);

		this->tt1 = static_cast<CEGUI::Tooltip*> (wmgr.createWindow("AlfiskoSkin/Label","tt1"));
		this->tt1->setPosition(CEGUI::UVector2(CEGUI::UDim(-0.05,0),CEGUI::UDim(0.075,0)));
		this->tt1->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->tt1->setText("Selected Files Between");
		this->w2->addChild(this->tt1);

		this->d1 = static_cast<CEGUI::Editbox*> (wmgr.createWindow("AlfiskoSkin/Editbox","eb1"));
		this->d1->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.105,0)));
		this->d1->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->d1->setText("02-04-2014");
		this->w2->addChild(this->d1);
		
		this->d2 = static_cast<CEGUI::Editbox*> (wmgr.createWindow("AlfiskoSkin/Editbox","eb2"));
		this->d2->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.137,0)));
		this->d2->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->d2->setText("08-12-2014");
		this->w2->addChild(this->d2);

		this->tt2 = static_cast<CEGUI::Tooltip*> (wmgr.createWindow("AlfiskoSkin/Label","tt2"));
		this->tt2->setPosition(CEGUI::UVector2(CEGUI::UDim(-0.071,0),CEGUI::UDim(0.170,0)));
		this->tt2->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->tt2->setText("Sort files by:");
		this->w2->addChild(this->tt2);

		this->tt3 = static_cast<CEGUI::Tooltip*> (wmgr.createWindow("AlfiskoSkin/Label","tt3"));
		this->tt3->setPosition(CEGUI::UVector2(CEGUI::UDim(-0.071,0),CEGUI::UDim(0.125,0)));
		this->tt3->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->tt3->setText("Enter directory:");
		this->sheet->addChild(this->tt3);

		this->directory = static_cast<CEGUI::Editbox*> (wmgr.createWindow("AlfiskoSkin/Editbox","directory"));
		this->directory->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.157,0)));
		this->directory->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->directory->setText("C:/");
		this->sheet->addChild(this->directory);


		this->scan = static_cast<CEGUI::Tooltip*> (wmgr.createWindow("AlfiskoSkin/Button","scan"));
		this->scan->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.215,0)));
		this->scan->setSize(CEGUI::USize(CEGUI::UDim(0.05, 0), CEGUI::UDim(0.035, 0)));
		this->scan->setText("SCAN");
		this->scan->deactivate();
		this->sheet->addChild(this->scan);

		this->rb1 = static_cast<CEGUI::RadioButton*> (wmgr.createWindow("AlfiskoSkin/RadioButton","rb1"));
		this->rb1->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.2,0)));
		this->rb1->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->rb1->setText("Last Access Date");
		this->rb1->setGroupID(1);
		this->rb1->setID(1);
		this->rb1->setSelected(true);
		this->rb1->setVisible(true);
		
		this->w2->addChild(this->rb1);

		this->rb2 = static_cast<CEGUI::RadioButton*> (wmgr.createWindow("AlfiskoSkin/RadioButton","rb2"));
		this->rb2->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.225,0)));
		this->rb2->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->rb2->setText("Last Modification Date");
		this->rb2->setGroupID(1);
		this->rb2->setID(2);
		this->rb2->setSelected(false);
		this->rb2->setVisible(true);
		
		this->w2->addChild(this->rb2);

		this->rb3 = static_cast<CEGUI::RadioButton*> (wmgr.createWindow("AlfiskoSkin/RadioButton","rb3"));
		this->rb3->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.250,0)));
		this->rb3->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->rb3->setText("Creation Date");
		this->rb3->setGroupID(1);
		this->rb3->setID(3);
		this->rb3->setSelected(false);
		this->rb3->setVisible(true);
		
		this->w2->addChild(this->rb3);

		//toggle button 
		this->tb = static_cast<CEGUI::ToggleButton*> (wmgr.createWindow("AlfiskoSkin/Checkbox","toggle"));
		this->tb->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.3,0)));
		this->tb->setSize(CEGUI::USize(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.025, 0)));
		this->tb->setText("Sort in descending order");
		this->w2->addChild(this->tb);

		this->RenderButton = static_cast<CEGUI::PushButton*> (wmgr.createWindow("AlfiskoSkin/Button","RenderButton"));
		this->RenderButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.350,0)));
		this->RenderButton->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.035, 0)));
		this->RenderButton->setText("RENDER");
		this->RenderButton->deactivate();
		this->w2->addChild(this->RenderButton);

		this->w2->show();
		this->sheet->addChild(this->w2);
		this->sheet->show();
		isVisible = true;

		CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(this->sheet);
	}

public:

	static bool isVisible;
	//GUI Elements
	static CEGUI::Window *sheet;
	//static text fields
	CEGUI::Tooltip* tt1;
	CEGUI::Tooltip* tt2;
	CEGUI::Tooltip* tt3;

	CEGUI::Window* label1;
	CEGUI::Window* label2;

	CEGUI::Window* w1;
	CEGUI::Window* w2;

	CEGUI::Window* FPSLabel;
	CEGUI::Window* scan;
	CEGUI::Window* Logo;
	CEGUI::PushButton* RenderButton;

	//editboxes for text inputs
	CEGUI::Editbox* d1;
	CEGUI::Editbox* d2;

	CEGUI::Editbox* directory;

	//radio buttons
	CEGUI::RadioButton* rb1;
	CEGUI::RadioButton* rb2;
	CEGUI::RadioButton* rb3;
	//check box
	CEGUI::ToggleButton* tb;

	CEGUI::ProgressBar* progress_bar;

	static GUI* getInstance();

	static void showHideGUI();

};