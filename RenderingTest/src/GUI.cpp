#include "GUI.h"


GUI* GUI::instance;
CEGUI::Window* GUI::sheet;
bool GUI::isVisible;

GUI* GUI::getInstance()
{

	if (instance == NULL)
		instance = new GUI();
	return instance;
}

void GUI::showHideGUI()
{
	if (isVisible)
	{
		GUI::getInstance()->w2->setVisible(false);
		GUI::getInstance()->scan->setVisible(false);
		GUI::getInstance()->directory->setVisible(false);
		GUI::getInstance()->tt3->setVisible(false);
		GUI::getInstance()->label1->setVisible(false);
		isVisible = false;
	}
	else
	{
		GUI::getInstance()->w2->setVisible(true);
		GUI::getInstance()->scan->setVisible(true);
		GUI::getInstance()->directory->setVisible(true);
		GUI::getInstance()->tt3->setVisible(true);
		GUI::getInstance()->label1->setVisible(true);
		isVisible = true;
	}



	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setVisible(CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().isVisible() ? false : true );
}