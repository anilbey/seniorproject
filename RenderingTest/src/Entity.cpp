#include "Entity.h"

Entity* Entity::instance;

Entity* Entity::getInstance()
{
	return (instance == NULL) ? new Entity() : instance;
}

bool Entity::isTextureFile()
{

	if (this->extension.compare("PNG") || this->extension.compare("JPG") || this->extension.compare("JPEG") || this->extension.compare("GIF"))
		return true;
	else
		return false;
}

int Entity::monthNumber (std::string month)
{
	return ((month.compare("Jan")) ? 1 : (month.compare("Feb")) ? 2 : (month.compare("Mar")) ? 3 : (month.compare("Apr")) ? 4 :
		(month.compare("May")) ? 5 : (month.compare("Jun")) ? 6 : (month.compare("Jul")) ? 7 : (month.compare("Aug")) ? 8 :
		(month.compare("Sep")) ? 9 : (month.compare("Oct")) ? 10 : (month.compare("Nov")) ? 11 : 12);
}



Entity::~Entity()
{
	name = NULL;
	
}

void Entity::setExtension()
{

	int lastDotIndex = 0;

	std::string str(name);

	for (int i = strlen(name) - 1; i > 0; i--)
	{
		if (name[i] == '.')
		{
			lastDotIndex = i;
			
			goto found;
		}	
	}

	// not found statement
	extension = "";
	return;

	found:
	std::string str2 = "";

	for (int i = lastDotIndex; i < (strlen(name) -1) ; i++)
	{
		str2 += str[i+1];
	}

	extension = str2;
	
}