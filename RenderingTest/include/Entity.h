#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <time.h>

struct Date
{
	unsigned short year,month,day;
	unsigned long date;
};


//Singleton Pattern
class Entity
{
private:

	static Entity* instance;

	Entity::Entity()
	{
		size = c = a = m = 0;
		write_permission = access_permission = 0;
	}	

public:

	
	const char* full_name;
	char* name;
	const char* directory;
	unsigned long size;
	std::string extension;
	unsigned short write_permission;
	unsigned short access_permission;
	char* creation_time;
	char* access_time;
	char* modification_time;
	unsigned long c, a, m;  //create, access, modification

	//Methods

	static Entity* getInstance();
	~Entity();
	int monthNumber (std::string);
	void setExtension();
	bool isTextureFile();

	unsigned long parseDate (char* time)
	{
		Date date;
		char* value = (char*) malloc (strlen (time) +2 );
		sprintf(value, "%s", time);
		char* dummy;
		dummy = strtok (value, " "); // day (mon, tue, wed etc. (ignored)
		dummy = strtok (NULL, " "); // month
		date.month = monthNumber(dummy);
		dummy = strtok (NULL, " "); // day (integer)
		date.day = atoi (dummy);
		dummy = strtok (NULL, " "); // hour minute second (ignored, already in creation_time)
		dummy = strtok (NULL, " "); // year
		date.year = atoi (dummy);

		date.date = (date.year * 10000) + (date.month * 100) + date.day;
		free (value);
		value = dummy = NULL;
		return date.date;
	}

};

